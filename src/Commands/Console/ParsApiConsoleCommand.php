<?php

namespace Commands\Console;

use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class ParsApiConsoleCommand extends Command
{
    protected function configure()
    {
        $this->setName('get-api')
            ->setDescription('Retrieving data from api [freelancehunt.com]!')
            ->setHelp('Retrieving data from api.freelancehunt.com');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $pdo = $this->connectPdo();
        $pdo->query("TRUNCATE TABLE `project`");
        echo "\n<======> Success!!! Records received - " . $this->getData($pdo) ." items! <======> \n";
    }

    public function insertDataPdo($name_project, $link, $budget, $user_name, $user_login, \PDO $pdo): void
    {
        $pdo->query("
              INSERT INTO `project`(`name_project`, `link`, `budget`, `user_name`, `user_login`)
              VALUES
              (
                \"$name_project\",
                \"$link\",
                \"$budget\",
                \"$user_name\",
                \"$user_login\"
              )
        ");
    }

    private function getData($pdo)
    {
        $k = 1;
        do {
            $resultArray = $this->getArrayCurl($k);

//            sleep(1);

            foreach ($resultArray['data'] as $item) {
                $project = isset($item['attributes']['name']) ? $item['attributes']['name'] : '';
                $project = str_replace('"', '', $project);
                stripslashes($project);

                $firstName = isset($item['attributes']['employer']['first_name']) ? $item['attributes']['employer']['first_name'] : '';
                $lastName = isset($item['attributes']['employer']['last_name']) ? $item['attributes']['employer']['last_name'] : '';
                $name = $firstName . ' ' . $lastName;
                $this->insertDataPdo(
                    $project,
                    isset($item['links']['self']['web']) ? $item['links']['self']['web'] : '',
                    isset($item['budget']) ? $item['budget'] : '',
                    $name,
                    isset($item['attributes']['employer']['login']) ? $item['attributes']['employer']['login'] : '',
                    $pdo
                );
            }

            if (array_key_exists('last', $resultArray['links'])) {
                $totalPage = $this->getTotalPage($resultArray['links']['last']);
            } else {
                $totalPage = 0;
            }

            print_r('Iteration:'.$k.'. Total:'.$totalPage.'. Count:'.count($resultArray['data'])."\n");
        } while ($k++<=$totalPage);

        return ($this->getTotalPage($resultArray['links']['prev']) * 10) + count($resultArray['data']) ;
    }

    private function getArrayCurl($i = 1)
    {
//        $i = 24;
        $url = "https://api.freelancehunt.com/v2/projects?page[number]={$i}";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        $headers = [];
        $headers[] = "Content-Type: application/json";
        $headers[] = "Accept: application/json";
        $headers[] = "Authorization: Bearer d2ab6142f6ca2b42b532c381962fc5bd0374bd79";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $code = (int)$code;
        $errors = [
            301 => 'Moved permanently',
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable'
        ];
        try {
            if ($code >= 400)
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
        } catch (Exception $E) {
            die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
        }

        $resultArray = json_decode($result, true);
        return $resultArray;
    }

    private function getTotalPage($last)
    {
        return str_replace(
            substr(
                $last,
                0,
                (stripos($last, "=") + 1)),
            '',
            $last);
    }

    protected function connectPdo(): \PDO
    {
        $host = '127.0.0.1';
        $db = 'pars_api';
        $user = 'root';
        $pass = 'root';
        $charset = 'utf8mb4';
        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES => false,
        ];
        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";

        try {
            $pdo = new \PDO($dsn, $user, $pass, $options);
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
        return $pdo;
    }
}
