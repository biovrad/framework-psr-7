<?php

use App\Http\Action;
use Framework\Http\ResponseSender;
use Framework\Http\Router\Exception\RequestNotMatchedException;
use Framework\Http\Router\RouteCollection;
use Framework\Http\Router\Router;
use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\ServerRequestFactory;

require dirname(__DIR__).'/vendor/autoload.php';

### Initialization

$routes = new RouteCollection();

$params = [
    'users' => [
        'admin' => 'password'
    ],
];

$routes->get('home', '/', new Action\HelloAction());
$routes->get('about', '/about', new Action\AboutAction());
$routes->get('blog', '/blog', new Action\Blog\IndexAction());
$routes->get('blog_show', '/blog/{id}', new Action\Blog\ShowAction(), ['id' => '\d+']);
$routes->get('cabinet', '/cabinet', new Action\CabinetAction($params['users']));


$router = new Router($routes);
$request = ServerRequestFactory::fromGlobals();

### Running

try {
    $result = $router->match($request);
    foreach ($result->getAttributes() as $attribute => $value) {
        $request = $request->withAttribute($attribute, $value);
    }
    /** @var callable $action */
    $action = $result->getHandler();
    $response = $action($request);
} catch (RequestNotMatchedException $e){
    $response = new HtmlResponse('Undefined page', 404);
}

### Postprocessing
$response = $response->withHeader('X-Developer', 'ElisDN');

### Sending
$emitter = new ResponseSender();
$emitter->send($response);
